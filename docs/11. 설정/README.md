###maven 프로젝트 생성
```
mvn archetype:generate -DgroupId="com.newlecture" -DartifactId=javaprj -DarchetypeArtifactId=maven-archetype-quickstart
```

### plugin 변경
```
  <build>
    <plugins>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

### 버전 설정
```
  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
  </properties>
```